#include <linux/linkage.h>
#include <linux/moduleloader.h>
#include "xstruct.h"
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <crypto/hash.h>
#include <linux/err.h>

#define XCRYPT_BLOCK_SIZE PAGE_SIZE 
#define PAD_SIZE 16
#define HASH_SIZE 32

typedef struct XCryptPreamble
{
	char check_sum[HASH_SIZE];
	struct crypto_blkcipher *blkcipher;
	char *cipher;
	struct blkcipher_desc desc;
}XPreamble;

asmlinkage extern long (*sysptr)(void *args);


/*
xhash -MD5 Hash input key. 
@key: key pass from user-level.
@key_len: length of key.
store Hash value in check_sum field in XPreamble.
return 0 if no error.
Modified from
https://www.kernel.org/doc/Documentation/crypto/api-intro.txt
*/
int xhash(const void *key, size_t key_len, XPreamble *preamble){
	int ret=-EINVAL;
	struct scatterlist sg[1];
    struct crypto_hash *tfm;
    struct hash_desc desc;
    tfm = crypto_alloc_hash("md5", 0, CRYPTO_ALG_ASYNC);
    if (IS_ERR(tfm)){
		ret= -EINVAL;
		goto freehash;
	}
    desc.tfm = tfm;
    desc.flags = 0;
    sg_init_table(sg,1);
	sg_set_buf(sg,key,key_len);
	ret= crypto_hash_digest(&desc, sg, sg->length, preamble->check_sum);
    if (ret < 0){		
		pr_err("Hash Error. %d\n", ret);
		ret=-EINVAL;
		goto freehash;
	}
	preamble->check_sum[HASH_SIZE]='\0';            
freehash:
    crypto_free_hash(tfm);
	return ret;
}


/*
xencrypt - encrypt plaintext to ciphertext
@xpreamble check_sum field of XPreamble store the hashed key to encryt plaintext.
@dst location where ciphertext stored.
@dst_len length of ciphertext.
@src location where plaintext stored.
@src_len length of plaintext.
return 0 if no error.
Modified from 
http://www.chronox.de/crypto-API/ch06s02.html
http://lxr.fsl.cs.sunysb.edu/linux/source/net/ceph/crypto.c
*/
int xencrypt(XPreamble *xpreamble, void *dst, size_t *dst_len, const void *src, size_t src_len)
{
	int ret = -EINVAL;
	//unsigned int ivsize = 0;
	struct scatterlist sg_in[2], sg_out[1];
	size_t zero_padding = (0x10 - (src_len & 0x0f));
	char pad[PAD_SIZE];
	//ivsize = crypto_blkcipher_ivsize(blkcipher);
	//if (ivsize) {
	//	if (ivsize != strlen(iv))
	//		printk("IV length differs from expected length\n");
	//	crypto_blkcipher_set_iv(blkcipher, iv, ivsize);
	//}
	memset(pad, zero_padding, zero_padding);
	*dst_len = src_len +  zero_padding;
	sg_init_table(sg_in,2);
	sg_set_buf(&sg_in[0],src,src_len);
	sg_set_buf(&sg_in[1],pad,zero_padding);
	sg_init_table(sg_out,1);
	sg_set_buf(sg_out,dst,*dst_len);
	ret = crypto_blkcipher_encrypt(&(*xpreamble).desc, sg_out, sg_in, *dst_len);
	if (ret < 0){
		pr_err("Encrypt failed %d\n", ret);
		ret=-EINVAL;
	}
	return ret;
}


/*
xdecrypt - decrypt plaintext to ciphertext
@xpreamble check_sum field of XPreamble store the hashed key to decryt ciphertext.
@dst location where plaintext stored.
@dst_len length of plaintext.
@src location where ciphertext stored.
@src_len length of ciphertext.
return 0 if no error.
Modified from 
http://www.chronox.de/crypto-API/ch06s02.html
http://lxr.fsl.cs.sunysb.edu/linux/source/net/ceph/crypto.c
*/
int xdecrypt(XPreamble* xpreamble, void *dst, size_t *dst_len, const void *src, size_t src_len)
{
	int ret = -EINVAL;
	struct scatterlist sg_in[1], sg_out[2];
	char pad[PAD_SIZE];
	int last_byte;
	sg_init_table(sg_in, 1);
	sg_set_buf(sg_in, src, src_len);
	sg_init_table(sg_out, 2);
	sg_set_buf(&sg_out[0], dst, *dst_len);
	sg_set_buf(&sg_out[1], pad, sizeof(pad));
	ret = crypto_blkcipher_decrypt(&(*xpreamble).desc, sg_out, sg_in, src_len);
	if (ret < 0) {
		pr_err("Decrypt failed %d\n", ret);
		ret=-EINVAL;
		return ret;
	}
	if (src_len <= *dst_len)
		last_byte = ((char *)dst)[src_len - 1];
    else
		last_byte = pad[src_len - *dst_len - 1];
    if (last_byte <= 16 && src_len >= last_byte) {
		*dst_len = src_len - last_byte;
    } 
	else{
		pr_err("Bad padding %d on src len %d\n",
        last_byte, (int)src_len);
        ret=-504;  /* bad padding */
    }
	return ret;
}

/*
copy_args_from_user - copy user level arguments to kernel level	
@kxargs: kernel level XArgs.
@args: user level XArgs  
return 0 if no error.
*/
int copy_args_from_user(XArgs* kxargs, void* args){
	XArgs* uxargs=(XArgs*) args;
	if (uxargs == NULL)
		return -EFAULT;
	if (copy_from_user( kxargs->xin, uxargs->xin, strlen(uxargs->xin) )!=0)
		return -EFAULT;
	kxargs->xin[strlen(uxargs->xin)]='\0';
	if (copy_from_user( kxargs->xout, uxargs->xout, strlen(uxargs->xout) )!=0)
		return -EFAULT;
	kxargs->xout[strlen(uxargs->xout)]='\0';
	if (copy_from_user( kxargs->xkeybuf, uxargs->xkeybuf, strlen(uxargs->xkeybuf) )!=0)
		return -EFAULT;
	kxargs->xkeybuf[strlen(uxargs->xkeybuf)]='\0';
	kxargs->xkeylen=uxargs->xkeylen;
	kxargs->xflag=uxargs->xflag;
	return 0;
}


/*
initialize_preamble - set up XPreamble to store cipher information
@key: set key for crypto_cipher
@key_len: length of key
@@xpreamble: store cipher information in XPreamble
return 0 if no error.
*/
int initialize_preamble(const void* key, size_t key_len, XPreamble* xpreamble ){
	xpreamble->cipher="cbc(aes)";
	xpreamble->blkcipher = crypto_alloc_blkcipher(xpreamble->cipher, 0, CRYPTO_ALG_ASYNC);
	if (IS_ERR(xpreamble->blkcipher)){
		printk("Could not allocate blkcipher handle for %s\n", xpreamble->cipher);
		return -EFAULT;
	}
	if (crypto_blkcipher_setkey(xpreamble->blkcipher, key, key_len)){
		printk("Key could not be set\n");
		return -EAGAIN;
	}
	xpreamble->desc.flags = 0;
	xpreamble->desc.tfm = xpreamble->blkcipher;
	return 0;
}


/*
validate_key - check if stored key in file is same as user input key.
@decryptkey: hashed binary key stored in file.
@userkey: hashed binary key generated from user input.
return 1 if two keys are the same, 0 if different.
*/
int validate_key(char *decryptkey, char *userkey){
	int i;
	for (i=0;i<HASH_SIZE;i++ ){
		if(decryptkey[i]!=userkey[i])
		return 0;
	}
	return 1;
}


/*
delete_file - delete outfile if error occurs.
@delfile: file which needs to be deleted.
@recovery: indicate if outfile needs to be recovered. 1 for yes.
return 0 if no error.
*/
int delete_file(struct file * delfile, int recovery){
	if (delfile->f_path.dentry != NULL && delfile->f_path.dentry->d_parent->d_inode!=NULL)
		vfs_unlink(delfile->f_path.dentry->d_parent->d_inode, delfile->f_path.dentry,NULL );
	//printk("file inode %d\n",delfile->f_path.dentry->d_parent->d_inode->i_ino );
	return 0;
}


asmlinkage long xcrypt(void *args)
{
	int ret=-EINVAL;
	struct file *infilp=NULL;
	struct file *outfilp=NULL;
	struct file *testfilp=NULL;
	char *src;
	char *dst;
	char *key;
	int recovery;	
	XArgs *kxargs;
	XPreamble *xpreamble;
	
	int bytes;
	size_t in_block_size;
	size_t out_block_size;
	mm_segment_t oldfs;
	
	kxargs=kmalloc(sizeof(XArgs), GFP_KERNEL);
	ret=copy_args_from_user(kxargs,args);
	if(ret!=0)
		goto freexargs;	
	infilp = filp_open(kxargs->xin, O_RDONLY, S_IRUSR);
	if (!infilp || IS_ERR(infilp)) {
		printk("wrapfs open infile err %d\n", (int) PTR_ERR(infilp));
		ret=-ENOENT;
		goto freexargs;
	}
	if (!S_ISREG(infilp->f_path.dentry->d_inode->i_mode)){
		ret=-EISDIR;
		goto closeinfile;
	}
	if (!infilp->f_op->read){
		ret=-EACCES;  // file(system) doesn't allow read 
		goto closeinfile;
	}	
	
	bytes=infilp->f_path.dentry->d_inode->i_size;
	if (bytes==0){
		printk("Empty infile\n");
		ret=-501;
		goto closeinfile;
	}
	recovery=0;
	testfilp = filp_open(kxargs->xout, O_RDONLY, S_IRUSR);
	if (!IS_ERR(testfilp)){
		recovery=1;
		if(infilp->f_path.dentry->d_inode->i_ino == testfilp->f_path.dentry->d_inode->i_ino){
			ret = -502;
			filp_close(testfilp, NULL);
			goto closeinfile;
		}
		filp_close(testfilp, NULL);
	}
	outfilp = filp_open(kxargs->xout,  O_CREAT | O_RDWR | O_TRUNC, infilp->f_path.dentry->d_inode->i_mode);
	if (!outfilp || IS_ERR(outfilp)){
		printk("wrapfs open outfile err %d\n", (int) PTR_ERR(outfilp));
		ret=-ENOENT;  // or do something else 
		goto closeinfile;
	}
	if (!S_ISREG(outfilp->f_path.dentry->d_inode->i_mode)){
		ret=-EISDIR;
		goto closeoutfile;
	}
	if (!outfilp->f_op->write){
		ret= -EACCES;  //file(system) doesn't allow write 
		goto closeoutfile;
	}

	if (kxargs->xflag==1){
		in_block_size=XCRYPT_BLOCK_SIZE;
		out_block_size=XCRYPT_BLOCK_SIZE+PAD_SIZE;
	}
	else{
		in_block_size=XCRYPT_BLOCK_SIZE+PAD_SIZE;
		out_block_size=XCRYPT_BLOCK_SIZE;
	}
	xpreamble=kmalloc(sizeof(XPreamble), GFP_KERNEL);
	ret=xhash(kxargs->xkeybuf,kxargs->xkeylen,xpreamble);
	if(ret!=0)
		goto freepreamble;
	ret=initialize_preamble(kxargs->xkeybuf,kxargs->xkeylen,xpreamble);
	if(ret!=0)
		goto freepreamble;
	src=kmalloc(XCRYPT_BLOCK_SIZE+PAD_SIZE, GFP_KERNEL);
	dst=kmalloc(XCRYPT_BLOCK_SIZE+PAD_SIZE, GFP_KERNEL);
	infilp->f_pos = 0;
	outfilp->f_pos = 0;
	oldfs = get_fs();
	
	set_fs(KERNEL_DS);
	if (kxargs->xflag==1){
		bytes=outfilp->f_op->write(outfilp, (xpreamble->check_sum),HASH_SIZE, &outfilp->f_pos);
	}
	else{
		key=kmalloc(HASH_SIZE,GFP_KERNEL);
		bytes=infilp->f_op->read(infilp, key, HASH_SIZE, &infilp->f_pos);
		if(validate_key(key,(xpreamble->check_sum))!=1){
			printk("Wrong Key\n");
			ret=-ENOKEY;
			kfree(key);
			goto out;
		}
		kfree(key);
	}
	if (bytes!=HASH_SIZE){	
		delete_file(outfilp,recovery);
		goto out;
	}
	while ((bytes = infilp->f_op->read(infilp, src, in_block_size, &infilp->f_pos))>0){
		if(kxargs->xflag==1){
			ret=xencrypt(xpreamble, dst, &out_block_size, src, bytes);
			if (ret!=0){
				delete_file(outfilp,recovery);
				goto out;	
			}
		}
		else{
			ret=xdecrypt(xpreamble, dst, &out_block_size, src, bytes);
			if (ret!=0){
				delete_file(outfilp,recovery);
				goto out;
			}
		}
		bytes=outfilp->f_op->write(outfilp, dst,out_block_size, &outfilp->f_pos);
		if (bytes!=out_block_size){
			delete_file(outfilp,recovery);
			goto out;
		}
	}
	
	set_fs(oldfs);
	ret=0;
out:
	if(src)
		kfree(src);
	if(dst)
		kfree(dst);
freepreamble:
	if(xpreamble)
		kfree(xpreamble);
closeoutfile:
	if(outfilp)
		filp_close(outfilp, NULL);
closeinfile:
	if(infilp)
		filp_close(infilp, NULL);	
freexargs:
	if(kxargs)
		kfree(kxargs); 
	//printk("return %d\n",ret);
	return ret;
}

static int __init init_sys_xcrypt(void)
{
	printk("installed new sys_xcrypt module\n");
	if (sysptr == NULL)
	sysptr = xcrypt;
	return 0;
}
static void  __exit exit_sys_xcrypt(void)
{
	if (sysptr != NULL)
	sysptr = NULL;
	printk("removed sys_xcrypt module\n");
}
module_init(init_sys_xcrypt);
module_exit(exit_sys_xcrypt);
MODULE_LICENSE("GPL");
