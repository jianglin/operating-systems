#ifndef _xstruct_h
#define _xstruct_h

#define MAX_FILE_PATH_LENGTH 256
#define KEYBUF_LENGTH 16

typedef struct XCryptArgs
{
  char	xin[MAX_FILE_PATH_LENGTH];
  char	xout[MAX_FILE_PATH_LENGTH];
  char	xkeybuf[KEYBUF_LENGTH];
  int xkeylen;
  int xflag;
  
}XArgs;


#endif
