#include <asm/unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <string.h>
#include "xstruct.h"

#include <openssl/hmac.h>
#include <openssl/md5.h>

#ifndef __NR_xcrypt
#error xcrypt system call not defined
#endif


int xcipher(XArgs x_args){
	int rc;
	rc = syscall(__NR_xcrypt, &x_args);
	if (rc == 0)
		printf("syscall returned addr: %d ", rc);
	else{
		printf("syscall returned %d (errno=%d)\n", rc, errno);
		switch(errno){
			case EFAULT:
				printf("Bad Memory Address Error.\n");
				break;
			case ENOENT:
				printf("File Not Exist.\n");
				break;
			case EACCES:
				printf("Access Denied. No Permission.\n");
				break;
			case EINVAL:
				printf("Wrong Function Arguments.\n");
				break;
			case EAGAIN:
				printf("Key Could Not Be Set.\n");
				break;	
			case ENOKEY:
				printf("Wrong Key.\n");
				break;
			case EISDIR:
				printf("Infile/Outfile is a directory.\n");
				break;
			case 501:
				printf("Empty Infile.\n");
				break;
			case 502:
				printf("Same Infile And Outfile.\n");
				break;
			case 503:
				printf("Same Infile And Outfile.\n");
				break;
			case 504:
				printf("Bad Padding.\n");
				break;
			default:
				printf("Unknown Error!\n");
				break;
		}
	}
	printf("xcipher call ends\n");
	return 0;
}

void help_message(void){
	printf("Usage:\n");
	printf(" -e to encrypt\n");
	printf(" -d to decrypt\n");
	printf(" -p \"key\" to specify key\n");
	printf(" -h to shwo help message\n");
	printf(" infile input file\n");
	printf(" outfile output file\n");
	exit (1);
}

int md5(char* result, const char* string){
	unsigned char digest[MD5_DIGEST_LENGTH];
    MD5((unsigned char*)string, strlen(string), (unsigned char*)&digest); 
	memcpy(result, digest, MD5_DIGEST_LENGTH);
	return 1;
}

int validate_argv(int argc, char *argv[], XArgs* xargs){
	char *options = ":edp:h";
	extern char *optarg;
	extern int optind, opterr, optopt;
	int opt=0;
	int edsum=0;
	int pwdcount=0;
	char* keybuf;
	const char* infile;
	const char* outfile;
	while((opt = getopt(argc,argv,options))!=-1){
		switch(opt){	
			case 'e':
				edsum++;
				xargs->xflag = 1;
				break;
			case 'd':
				edsum++;
				xargs->xflag=0;
				break;
			case 'p':
				pwdcount++;
				if (optarg==NULL){
					printf("Key Missing\n");
					return -EINVAL;
					}
				keybuf = optarg;
				if (strlen(keybuf)<6 ){
					printf("Key length error Minimum Length: 6\n");
					return 0;
					}
				else{
					char* md5hash;
					md5hash=malloc(MD5_DIGEST_LENGTH);
					md5(md5hash,keybuf);
					strcpy(xargs->xkeybuf,md5hash);
					xargs->xkeylen = MD5_DIGEST_LENGTH;
					free(md5hash);
					}
			break;
	       
		case 'h':
			help_message();
			break;
		case ':':
			fprintf(stderr,"Option -%c needs argument\n",optopt);
			return 0;
		case '?':
			fprintf(stderr,"Unknown option: -%c\n",optopt);
			return 0;
		default:
			return 0;
		}
	}
	if (edsum!=1){
		printf("One and Only One of -e and -d required.\n");
		return 0;
	}
	if (pwdcount!=1){
		printf("Missing Password Option.\n");
		return 0;
	}
	if(optind != argc-2){
		printf("One infile and One outfile are required.\n");
		return 0;
	}
	infile=argv[optind];
	optind++;
	outfile=argv[optind];
	if (strlen(infile)<1 || strlen(infile)>MAX_FILE_PATH_LENGTH){
		printf("Invalid Outfile PathLength\n");
		return 0;
	}
	else{
		strcpy(xargs->xin,infile);
		}
	if (strlen(outfile)<1 || strlen(outfile)>MAX_FILE_PATH_LENGTH){
		printf("Invalid Outfile PathLength\n");
		return 0;
	}
	else{
		strcpy(xargs->xout,outfile);
		}
    return 1;
}

int main(int argc,  char *argv[])
{
	XArgs xargs;
	if (validate_argv(argc, argv, &xargs)==1){
		xcipher(xargs);	
	}
  	else{
		printf("Argv Error.\n");
		exit(1);
	}
	return 0;
}



