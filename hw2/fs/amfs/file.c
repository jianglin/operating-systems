/*
 * Copyright (c) 1998-2014 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2014 Stony Brook University
 * Copyright (c) 2003-2014 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "amfs.h"
#include "amfsctl.h"


/*
write buffer in super_block to pattern_db
@sbi: struct amfs_sb_info to get buffer 
return 0 if success.
*/
int write_to_pattern_db(struct amfs_sb_info *sbi){
	int err;
	struct file *filp=NULL;
	mm_segment_t oldfs;
	int bytes;
	filp = filp_open(sbi->db_path, O_RDWR | O_TRUNC,S_IWUSR);
	filp->f_pos = 0;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	bytes=filp->f_op->write(filp, sbi->data_on_disk,sbi->data_on_disk_size, &filp->f_pos);
	set_fs(oldfs);
	err=0;
	return err;
}

/*
add new pattern to pattern list
@new: new pattern
@sbi: struct amfs_sb_info to the pattern list
return 0 if success	
*/
int add_to_pattern_db(char* new,struct amfs_sb_info *sbi){
	int err;
	struct amfs_pattern_element *new_pattern;
	struct amfs_pattern_element *pattern_element;
	char *buf;
	struct list_head *pos;
	int new_size;
	
	list_for_each(pos, sbi->pattern_list){
		 pattern_element= list_entry(pos, struct amfs_pattern_element, list);
		 if (strcmp(pattern_element->pattern,new)==0){
			err=1;
			goto found_add;
			}
	}
	new_pattern = kmalloc(sizeof(struct amfs_pattern_element), GFP_KERNEL);
	strcpy(new_pattern->pattern, new);
	new_pattern->len=strlen(new);
	list_add_tail(&new_pattern->list, sbi->pattern_list);
	new_size=sbi->data_on_disk_size+new_pattern->len+1; //1 for ending "\n"
	buf=list_to_buf(sbi->pattern_list, "\n", new_size);
	kfree(sbi->data_on_disk);
	sbi->data_on_disk=buf;
	sbi->data_on_disk_size=new_size;
	write_to_pattern_db(sbi);
	err=0;
	
found_add:
	return err;
}

/*
remove pattern from pattern list
@delete: pattern that needs to remove
@sbi: struct amfs_sb_info to the pattern list
return 0, if pattern not found.
1, if pattern is found and removed.
-EINVAL, if error.
*/
int remove_from_pattern_db(char* delete,struct amfs_sb_info *sbi){
	int err=-EINVAL;
	struct amfs_pattern_element *pattern_element;
	struct list_head *pos;
	struct list_head *temp;
	char *buf;
	int new_size;
	list_for_each_safe(pos, temp, sbi->pattern_list){
		 pattern_element= list_entry(pos, struct amfs_pattern_element, list);
		 if (strcmp(pattern_element->pattern,delete)==0){
			list_del(pos);
			new_size=sbi->data_on_disk_size-pattern_element->len-1; //1 for ending "\n"
			// if new_size==0, do some something.
			buf=list_to_buf(sbi->pattern_list, "\n", new_size);
			kfree(sbi->data_on_disk);
			sbi->data_on_disk=buf;
			sbi->data_on_disk_size=new_size;
			write_to_pattern_db(sbi);
			kfree(pattern_element);
			err=1;
			goto found_delete;
			}
	}
	err=0;
found_delete:
	return err;
}

/*
scan for malware
@buf: buffer that needs to be scaned
@file: file that needs to be scaned (to get super_block)
parameters passed by amfs_read/amfs_write
return 0, if no pattern found in buffer.
1, if a pattern is found in buffer.
-EINVAL, if error.
*/
int scan_for_malware(const char __user *buf, struct file *file){
	int err;
	struct super_block *sb;
	struct amfs_pattern_element *pattern_element;
	struct list_head *pattern_pos;
	
	printk("scan_for_malware: %s\n",file->f_path.dentry->d_name.name);
	
	sb = file->f_path.dentry->d_sb;
	if (!AMFS_SB(sb)){
		err = -EINVAL;
		goto out;
		}
	
	
	list_for_each(pattern_pos,AMFS_SB(sb)->pattern_list){
		pattern_element = list_entry(pattern_pos, struct amfs_pattern_element, list);
		//printk("%s,%d     %s,%d\n",source_element->pattern,source_element->len,pattern_element->pattern,pattern_element->len);
		if (strstr(buf,pattern_element->pattern)!=NULL){
			printk("Found pattern:%s!\n",pattern_element->pattern);		
			err=1;
			goto out;
			}
	}		
	err=0;
out:
	return err;
}

/*
hide bad file, I'm thinking to link dir with new dentry, then unlink the old dentry. 
Append ".malware_" to the "bad" file. So ls will not list the bad file.
And change the permission of the file so that user cannot access. 
*/
int hide_bad_file(struct dentry *dentry, struct inode *dir){
	//return amfs_link(dentry, dir,  new_dentry);
	return 0;
}

/*
setxattr/hide/setpermission for bad file.
@buf: buffer that wants to read/write.
@file: file that needs to be scaned (to get super_block)
return 0, if anti_malware process success.
<0, if an error occur.
*/
int amfs_anti_malware(struct file *file, const char __user *buf){
	int err=-EINVAL;
	struct file *lower_file;
	struct dentry *lower_dentry;
	char* val;
	struct dentry *dentry = file->f_path.dentry;
	lower_file = amfs_lower_file(file);
	lower_dentry = lower_file->f_path.dentry;
	
	val=kmalloc(2,GFP_KERNEL);
		
	err=lower_dentry->d_inode->i_op->getxattr(lower_dentry, MATTR, val, 2);
	val[1]='\0';
	if(err>0 && strcmp(val,BAD)==0){//"bad" xattr has been set
		err=-EBADF;            
		printk("File marked as bad!\n");
		goto out;
		}
	err=scan_for_malware(buf,file);
	if (err<0){
		printk("scan error\n");
		goto out;	
		}
	else if (err==1){   //pattern found
		err =lower_dentry->d_inode->i_op->setxattr(lower_dentry, MATTR, BAD, 2,0);
		hide_bad_file(dentry, dentry->d_inode);
		printk("set attr user.mal to BAD for bad file\n");
			if(err<0){
			printk("set val err=%d\n",err);
			goto out;}
		}
	else if (err==0){    //pattern not found
		err =lower_dentry->d_inode->i_op->setxattr(lower_dentry, MATTR, GOOD, 2,0);
		printk("set attr user.mal to GOOD\n");
			if(err<0){	
				printk("set val err=%d\n",err);
				goto out;
			}
		}
	err=0;
out:
	if(val) kfree(val);
	return err;
}
	
static ssize_t amfs_read(struct file *file, char __user *buf,
			   size_t count, loff_t *ppos)
{
	int err;
	struct file *lower_file;
	struct dentry *dentry = file->f_path.dentry;
	lower_file = amfs_lower_file(file);
			
	err=amfs_anti_malware(file, buf);
	if (err<0)
		goto out;
	
	err = vfs_read(lower_file, buf, count, ppos);
	/* update our inode atime upon a successful lower read */
	if (err >= 0)
		fsstack_copy_attr_atime(dentry->d_inode,
					file_inode(lower_file));
out:
	return err;
}

static ssize_t amfs_write(struct file *file, const char __user *buf,
			    size_t count, loff_t *ppos)
{
	int err;

	struct file *lower_file;
	struct dentry *dentry = file->f_path.dentry;
	lower_file = amfs_lower_file(file);
	
	err=amfs_anti_malware(file, buf);
	if (err<0)
		goto out;
	
	err = vfs_write(lower_file, buf, count, ppos);
	/* update our inode times+sizes upon a successful lower write */
	if (err >= 0) {
		fsstack_copy_inode_size(dentry->d_inode,
					file_inode(lower_file));
		fsstack_copy_attr_times(dentry->d_inode,
					file_inode(lower_file));
	}
out:
	return err;
}

static int amfs_readdir(struct file *file, struct dir_context *ctx)
{
	int err;
	struct file *lower_file = NULL;
	struct dentry *dentry = file->f_path.dentry;

	lower_file = amfs_lower_file(file);
	err = iterate_dir(lower_file, ctx);
	file->f_pos = lower_file->f_pos;
	if (err >= 0)		/* copy the atime */
		fsstack_copy_attr_atime(dentry->d_inode,
					file_inode(lower_file));
	return err;
}

static long amfs_unlocked_ioctl(struct file *file, unsigned int cmd,
				  unsigned long arg)
{
	long err = -ENOTTY;
	struct file *lower_file;
	char *buf;
	struct super_block *sb;
	lower_file = amfs_lower_file(file);
    	
	/* XXX: use vfs_ioctl if/when VFS exports it */
	if (!lower_file || !lower_file->f_op)
		goto out;
	if (lower_file->f_op->unlocked_ioctl)
		err = lower_file->f_op->unlocked_ioctl(lower_file, cmd, arg);

	/* some ioctls can change inode attributes (EXT2_IOC_SETFLAGS) */
	if (!err)
		fsstack_copy_attr_all(file_inode(file),
				      file_inode(lower_file));
	
	sb = file->f_path.dentry->d_sb;
    
	if (!AMFS_SB(sb)){
		err = -EINVAL;
		goto out;
    }
		
	buf=kmalloc(MAX_PATTERN_LEN,GFP_KERNEL);
		
	switch(cmd) {
	case AMFS_IOCTLLIST:	
		err=copy_to_user((char *)arg, AMFS_SB(sb)->data_on_disk, PAGE_SIZE);
		break;
	case AMFS_IOCTLADD:
		err=copy_from_user(buf, (char *)arg, MAX_PATTERN_LEN);
		err=add_to_pattern_db(buf,AMFS_SB(sb));
		break;
	case AMFS_IOCTLREMOVE:
		err=copy_from_user(buf, (char *)arg, MAX_PATTERN_LEN);
		err=remove_from_pattern_db(buf,AMFS_SB(sb));
		break;

	default:
		return -ENOTTY;
	}
	if(buf) kfree(buf);
					  
out:
	return err;
}

#ifdef CONFIG_COMPAT
static long amfs_compat_ioctl(struct file *file, unsigned int cmd,
				unsigned long arg)
{
	long err = -ENOTTY;
	struct file *lower_file;

	lower_file = amfs_lower_file(file);

	/* XXX: use vfs_ioctl if/when VFS exports it */
	if (!lower_file || !lower_file->f_op)
		goto out;
	if (lower_file->f_op->compat_ioctl)
		err = lower_file->f_op->compat_ioctl(lower_file, cmd, arg);

out:
	return err;
}
#endif

static int amfs_mmap(struct file *file, struct vm_area_struct *vma)
{
	int err = 0;
	bool willwrite;
	struct file *lower_file;
	const struct vm_operations_struct *saved_vm_ops = NULL;

	/* this might be deferred to mmap's writepage */
	willwrite = ((vma->vm_flags | VM_SHARED | VM_WRITE) == vma->vm_flags);

	/*
	 * File systems which do not implement ->writepage may use
	 * generic_file_readonly_mmap as their ->mmap op.  If you call
	 * generic_file_readonly_mmap with VM_WRITE, you'd get an -EINVAL.
	 * But we cannot call the lower ->mmap op, so we can't tell that
	 * writeable mappings won't work.  Therefore, our only choice is to
	 * check if the lower file system supports the ->writepage, and if
	 * not, return EINVAL (the same error that
	 * generic_file_readonly_mmap returns in that case).
	 */
	lower_file = amfs_lower_file(file);
	if (willwrite && !lower_file->f_mapping->a_ops->writepage) {
		err = -EINVAL;
		printk(KERN_ERR "amfs: lower file system does not "
		       "support writeable mmap\n");
		goto out;
	}

	/*
	 * find and save lower vm_ops.
	 *
	 * XXX: the VFS should have a cleaner way of finding the lower vm_ops
	 */
	if (!AMFS_F(file)->lower_vm_ops) {
		err = lower_file->f_op->mmap(lower_file, vma);
		if (err) {
			printk(KERN_ERR "amfs: lower mmap failed %d\n", err);
			goto out;
		}
		saved_vm_ops = vma->vm_ops; /* save: came from lower ->mmap */
	}

	/*
	 * Next 3 lines are all I need from generic_file_mmap.  I definitely
	 * don't want its test for ->readpage which returns -ENOEXEC.
	 */
	file_accessed(file);
	vma->vm_ops = &amfs_vm_ops;

	file->f_mapping->a_ops = &amfs_aops; /* set our aops */
	if (!AMFS_F(file)->lower_vm_ops) /* save for our ->fault */
		AMFS_F(file)->lower_vm_ops = saved_vm_ops;

out:
	return err;
}

static int amfs_open(struct inode *inode, struct file *file)
{
	int err = 0;
	struct file *lower_file = NULL;
	struct path lower_path;

	/* don't open unhashed/deleted files */
	if (d_unhashed(file->f_path.dentry)) {
		err = -ENOENT;
		goto out_err;
	}

	file->private_data =
		kzalloc(sizeof(struct amfs_file_info), GFP_KERNEL);
	if (!AMFS_F(file)) {
		err = -ENOMEM;
		goto out_err;
	}

	/* open lower object and link amfs's file struct to lower's */
	amfs_get_lower_path(file->f_path.dentry, &lower_path);
	lower_file = dentry_open(&lower_path, file->f_flags, current_cred());
	path_put(&lower_path);
	if (IS_ERR(lower_file)) {
		err = PTR_ERR(lower_file);
		lower_file = amfs_lower_file(file);
		if (lower_file) {
			amfs_set_lower_file(file, NULL);
			fput(lower_file); /* fput calls dput for lower_dentry */
		}
	} else {
		amfs_set_lower_file(file, lower_file);
	}

	if (err)
		kfree(AMFS_F(file));
	else
		fsstack_copy_attr_all(inode, amfs_lower_inode(inode));
out_err:
	return err;
}

static int amfs_flush(struct file *file, fl_owner_t id)
{
	int err = 0;
	struct file *lower_file = NULL;

	lower_file = amfs_lower_file(file);
	if (lower_file && lower_file->f_op && lower_file->f_op->flush) {
		filemap_write_and_wait(file->f_mapping);
		err = lower_file->f_op->flush(lower_file, id);
	}

	return err;
}

/* release all lower object references & free the file info structure */
static int amfs_file_release(struct inode *inode, struct file *file)
{
	struct file *lower_file;

	lower_file = amfs_lower_file(file);
	if (lower_file) {
		amfs_set_lower_file(file, NULL);
		fput(lower_file);
	}

	kfree(AMFS_F(file));
	return 0;
}

static int amfs_fsync(struct file *file, loff_t start, loff_t end,
			int datasync)
{
	int err;
	struct file *lower_file;
	struct path lower_path;
	struct dentry *dentry = file->f_path.dentry;

	err = __generic_file_fsync(file, start, end, datasync);
	if (err)
		goto out;
	lower_file = amfs_lower_file(file);
	amfs_get_lower_path(dentry, &lower_path);
	err = vfs_fsync_range(lower_file, start, end, datasync);
	amfs_put_lower_path(dentry, &lower_path);
out:
	return err;
}

static int amfs_fasync(int fd, struct file *file, int flag)
{
	int err = 0;
	struct file *lower_file = NULL;

	lower_file = amfs_lower_file(file);
	if (lower_file->f_op && lower_file->f_op->fasync)
		err = lower_file->f_op->fasync(fd, lower_file, flag);

	return err;
}

static ssize_t amfs_aio_read(struct kiocb *iocb, const struct iovec *iov,
			       unsigned long nr_segs, loff_t pos)
{
	int err = -EINVAL;
	struct file *file, *lower_file;

	file = iocb->ki_filp;
	lower_file = amfs_lower_file(file);
	if (!lower_file->f_op->aio_read)
		goto out;
	/*
	 * It appears safe to rewrite this iocb, because in
	 * do_io_submit@fs/aio.c, iocb is a just copy from user.
	 */
	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->aio_read(iocb, iov, nr_segs, pos);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode atime as needed */
	if (err >= 0 || err == -EIOCBQUEUED)
		fsstack_copy_attr_atime(file->f_path.dentry->d_inode,
					file_inode(lower_file));
out:
	return err;
}

static ssize_t amfs_aio_write(struct kiocb *iocb, const struct iovec *iov,
				unsigned long nr_segs, loff_t pos)
{
	int err = -EINVAL;
	struct file *file, *lower_file;

	file = iocb->ki_filp;
	lower_file = amfs_lower_file(file);
	if (!lower_file->f_op->aio_write)
		goto out;
	/*
	 * It appears safe to rewrite this iocb, because in
	 * do_io_submit@fs/aio.c, iocb is a just copy from user.
	 */
	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->aio_write(iocb, iov, nr_segs, pos);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode times/sizes as needed */
	if (err >= 0 || err == -EIOCBQUEUED) {
		fsstack_copy_inode_size(file->f_path.dentry->d_inode,
					file_inode(lower_file));
		fsstack_copy_attr_times(file->f_path.dentry->d_inode,
					file_inode(lower_file));
	}
out:
	return err;
}

/*
 * amfs cannot use generic_file_llseek as ->llseek, because it would
 * only set the offset of the upper file.  So we have to implement our
 * own method to set both the upper and lower file offsets
 * consistently.
 */
static loff_t amfs_file_llseek(struct file *file, loff_t offset, int whence)
{
	int err;
	struct file *lower_file;

	err = generic_file_llseek(file, offset, whence);
	if (err < 0)
		goto out;

	lower_file = amfs_lower_file(file);
	err = generic_file_llseek(lower_file, offset, whence);

out:
	return err;
}

/*
 * amfs read_iter, redirect modified iocb to lower read_iter
 */
ssize_t
amfs_read_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;

	lower_file = amfs_lower_file(file);
	if (!lower_file->f_op->read_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->read_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode atime as needed */
	if (err >= 0 || err == -EIOCBQUEUED)
		fsstack_copy_attr_atime(file->f_path.dentry->d_inode,
					file_inode(lower_file));
out:
	return err;
}

/*
 * amfs write_iter, redirect modified iocb to lower write_iter
 */
ssize_t
amfs_write_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;

	lower_file = amfs_lower_file(file);
	if (!lower_file->f_op->write_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->write_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode times/sizes as needed */
	if (err >= 0 || err == -EIOCBQUEUED) {
		fsstack_copy_inode_size(file->f_path.dentry->d_inode,
					file_inode(lower_file));
		fsstack_copy_attr_times(file->f_path.dentry->d_inode,
					file_inode(lower_file));
	}
out:
	return err;
}

const struct file_operations amfs_main_fops = {
	.llseek		= generic_file_llseek,
	.read		= amfs_read,
	.write		= amfs_write,
	.unlocked_ioctl	= amfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= amfs_compat_ioctl,
#endif
	.mmap		= amfs_mmap,
	.open		= amfs_open,
	.flush		= amfs_flush,
	.release	= amfs_file_release,
	.fsync		= amfs_fsync,
	.fasync		= amfs_fasync,
	.aio_read	= amfs_aio_read,
	.aio_write	= amfs_aio_write,
	.read_iter	= amfs_read_iter,
	.write_iter	= amfs_write_iter,
};

/* trimmed directory options */
const struct file_operations amfs_dir_fops = {
	.llseek		= amfs_file_llseek,
	.read		= generic_read_dir,
	.iterate	= amfs_readdir,
	.unlocked_ioctl	= amfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= amfs_compat_ioctl,
#endif
	.open		= amfs_open,
	.release	= amfs_file_release,
	.flush		= amfs_flush,
	.fsync		= amfs_fsync,
	.fasync		= amfs_fasync,
};

