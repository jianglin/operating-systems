/*
 * Copyright (c) 1998-2014 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2014 Stony Brook University
 * Copyright (c) 2003-2014 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "amfs.h"
#include <linux/module.h>


/*
 * There is no need to lock the amfs_super_info's rwsem as there is no
 * way anyone can have a reference to the superblock at this point in time.
 */
 
/*
strsep
http://sapiexamples.com/c/string/strsep.html
list example
http://www.makelinux.net/ldd3/chp-11-sect-5
*/
void print_list(struct list_head *pattern_list){
	struct amfs_pattern_element *pattern_element;
	struct list_head *pos;
	list_for_each(pos, pattern_list) {
				pattern_element = list_entry(pos, struct amfs_pattern_element, list);
				printk("Pattern:%s(%d)\n",pattern_element->pattern,pattern_element->len);
	}
	
}

char* list_to_buf(struct list_head *pattern_list, char *delim, int buf_size){
	char* buf;	
	struct amfs_pattern_element *pattern_element;
	struct list_head *pos;
	int offset=0;
	buf=kmalloc(buf_size,GFP_KERNEL);
	list_for_each(pos, pattern_list) {
				pattern_element = list_entry(pos, struct amfs_pattern_element, list);
				memcpy( (buf+offset),pattern_element->pattern,pattern_element->len);
				buf[offset+pattern_element->len]='\n';
				offset+=pattern_element->len+1;
	}
	
	buf[buf_size]='\0';
	//printk("list_to_buf:\n%s\n",buf);	
	return buf; 
}

struct list_head* buf_to_list(char *buf, char *delim,int *count, int *total_size)
{
	struct list_head *pattern_list;
	struct amfs_pattern_element *new_pattern;
	char *token;
    char *stringp;
	
	pattern_list=kmalloc(sizeof(struct list_head),GFP_KERNEL);
	INIT_LIST_HEAD(pattern_list);
	*count = 0;
	*total_size = 0;
    stringp = buf;
	while (stringp != NULL && strlen(stringp)>0) {
        token = strsep(&stringp, delim);
		new_pattern = kmalloc(sizeof(struct amfs_pattern_element), GFP_KERNEL);
        strcpy(new_pattern->pattern, token);
		new_pattern->len=strlen(token);
        (*count)=(*count)+1;
		//new_pattern len + 1 for '\n'
		(*total_size)=(*total_size)+new_pattern->len+1;
		printk("Pattern:%s(%d)\n",new_pattern->pattern,new_pattern->len);
		list_add_tail(&new_pattern->list, pattern_list);
		
	}
	printk("total size:%d\n",*total_size);
	return pattern_list;
    
}
int mount_pattdb(char* filepath, struct amfs_raw_data* ard){
	int err=-EINVAL;
	struct file *filp=NULL;
	char* buf;
	char* buf_copy;
	int bytes;
	int start;
	int end;
	
	mm_segment_t oldfs;
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	
	filp = filp_open(filepath, O_RDONLY, S_IRUSR);
	if (!filp || IS_ERR(filp)) {
		printk("open file err %d\n", (int) PTR_ERR(filp));
		err=-ENOENT;
		goto out;
	}
	if (!S_ISREG(filp->f_path.dentry->d_inode->i_mode)){
		err=-EISDIR;
		goto closefile;
	}
	if (!filp->f_op->read){
		err=-EACCES;  // file(system) doesn't allow read 
		goto closefile;
	}	
	bytes=filp->f_path.dentry->d_inode->i_size;
	if (bytes==0){
		printk("Empty infile\n");
		err=-501;
		goto closefile;
	}
	ard->db_path=kmalloc(MAX_PATTERN_LEN,GFP_KERNEL);
	strcpy(ard->db_path,filepath);
	buf=kmalloc(PAGE_SIZE, GFP_KERNEL);
	
	filp->f_pos = 0;
	start=0;
	end=0;
	while ((bytes = filp->f_op->read(filp, buf, PAGE_SIZE, &filp->f_pos))>0){
			printk("bytes %d\n",bytes);
			
			buf[bytes]='\0';
			buf_copy=kmalloc(bytes,GFP_KERNEL);
			strcpy(buf_copy,buf);
			ard->data_on_disk=buf_copy;
			(*ard).data_on_disk_size=bytes; 
	
	}
	set_fs(oldfs);
	err=0;
	
	if(buf) kfree(buf);
closefile:
	if(filp) filp_close(filp, NULL);	
out:
	return err;
	
}

int amfs_parse_options(char *options, char *path) 
{
	/*Need to check if options is a valid path*/
	int err=-EINVAL;
	strcpy(path,options+7); 
	err=0;
	return err;
	
	
}

static int amfs_read_super(struct super_block *sb, void *raw_data, int silent)
{
	int err;
	struct super_block *lower_sb;
	struct path lower_path;
	struct amfs_raw_data* ard;
	char *dev_name;
	struct inode *inode;
	
	int* count;
	int* size;	
	struct amfs_pattern_element *pattern_element;
	struct list_head *pos;
	printk("amfs_read_super\n");
	
	ard= (struct amfs_raw_data*) raw_data;
	dev_name = ard->dev_name;
	if (!dev_name) {
		printk(KERN_ERR
		       "amfs: read_super: missing dev_name argument\n");
		err = -EINVAL;
		goto out;
	}

	/* parse lower path */
	err = kern_path(dev_name, LOOKUP_FOLLOW | LOOKUP_DIRECTORY,
			&lower_path);
	if (err) {
		printk(KERN_ERR	"amfs: error accessing "
		       "lower directory '%s'\n", dev_name);
		goto out;
	}

	/* allocate superblock private data */
	sb->s_fs_info = kzalloc(sizeof(struct amfs_sb_info), GFP_KERNEL);
	
	if (!AMFS_SB(sb)) {
		printk(KERN_CRIT "amfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out_free;
	}
	count=kmalloc(sizeof(int), GFP_KERNEL);
	size=kmalloc(sizeof(int), GFP_KERNEL);
	AMFS_SB(sb)->data_on_disk=kmalloc(ard->data_on_disk_size,GFP_KERNEL);
	strcpy(AMFS_SB(sb)->data_on_disk,ard->data_on_disk);
	AMFS_SB(sb)->pattern_list = buf_to_list(ard->data_on_disk, "\n",count,size);
	AMFS_SB(sb)->db_path=kmalloc(MAX_PATTERN_LEN,GFP_KERNEL);
	strcpy(AMFS_SB(sb)->db_path,ard->db_path);
	AMFS_SB(sb)->data_on_disk_size=(*size); 
	
		
	/* set the lower superblock field of upper superblock */
	lower_sb = lower_path.dentry->d_sb;
	atomic_inc(&lower_sb->s_active);
	amfs_set_lower_super(sb, lower_sb);

	/* inherit maxbytes from lower file system */
	sb->s_maxbytes = lower_sb->s_maxbytes;

	/*
	 * Our c/m/atime granularity is 1 ns because we may stack on file
	 * systems whose granularity is as good.
	 */
	sb->s_time_gran = 1;

	sb->s_op = &amfs_sops;

	/* get a new inode and allocate our root dentry */
	inode = amfs_iget(sb, lower_path.dentry->d_inode);
	if (IS_ERR(inode)) {
		err = PTR_ERR(inode);
		goto out_sput;
	}
	sb->s_root = d_make_root(inode);
	if (!sb->s_root) {
		err = -ENOMEM;
		goto out_iput;
	}
	d_set_d_op(sb->s_root, &amfs_dops);

	/* link the upper and lower dentries */
	sb->s_root->d_fsdata = NULL;
	err = new_dentry_private_data(sb->s_root);
	if (err)
		goto out_freeroot;

	/* if get here: cannot have error */

	/* set the lower dentries for s_root */
	amfs_set_lower_path(sb->s_root, &lower_path);

	/*
	 * No need to call interpose because we already have a positive
	 * dentry, which was instantiated by d_make_root.  Just need to
	 * d_rehash it.
	 */
	d_rehash(sb->s_root);
	if (!silent)
		printk(KERN_INFO
		       "amfs: mounted on top of %s type %s\n",
		       dev_name, lower_sb->s_type->name);
	goto out; /* all is well */

	if(pos) kfree(pos);
	if(pattern_element) kfree(pattern_element);
	if(count) kfree(count);
	if(size) kfree(size);
	/* no longer needed: free_dentry_private_data(sb->s_root); */
out_freeroot:
	dput(sb->s_root);
out_iput:
	iput(inode);
out_sput:
	/* drop refs we took earlier */
	atomic_dec(&lower_sb->s_active);
	kfree(AMFS_SB(sb));
	sb->s_fs_info = NULL;
out_free:
	path_put(&lower_path);

out:
	return err;
}

struct dentry *amfs_mount(struct file_system_type *fs_type, int flags,
			    const char *dev_name, void *raw_data)
{
	int err;
	char *db_path;
	struct amfs_raw_data* ard;
	ard=kmalloc(sizeof(struct amfs_raw_data),GFP_KERNEL);
	ard->dev_name=kmalloc(MAX_PATTERN_LEN,GFP_KERNEL);
	strcpy(ard->dev_name,dev_name);
	
	db_path=kmalloc(MAX_PATTERN_LEN,GFP_KERNEL);
	err=amfs_parse_options((char*)raw_data,db_path);
	if (err<0)
		{
		printk("Ivalid option \n");
		goto fail;
		}
	err=mount_pattdb(db_path,ard);
	if (err<0)
	{printk("Fail to mount pattdb \n");
	goto fail;	
	}
	if(db_path) kfree(db_path);
	printk("dev_name:%s flags:%d \n",dev_name,flags);
	return mount_nodev(fs_type, flags, ard,
			   amfs_read_super);
fail:
	if(db_path) kfree(db_path);
	return ERR_PTR(err);
}

static struct file_system_type amfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= AMFS_NAME,
	.mount		= amfs_mount,
	.kill_sb	= generic_shutdown_super,
	.fs_flags	= 0,
};
MODULE_ALIAS_FS(AMFS_NAME);

static int __init init_amfs_fs(void)
{
	int err;

	pr_info("Registering amfs " AMFS_VERSION "\n");

	err = amfs_init_inode_cache();
	if (err)
		goto out;
	err = amfs_init_dentry_cache();
	if (err)
		goto out;
	err = register_filesystem(&amfs_fs_type);
out:
	if (err) {
		amfs_destroy_inode_cache();
		amfs_destroy_dentry_cache();
	}
	return err;
}

static void __exit exit_amfs_fs(void)
{
	amfs_destroy_inode_cache();
	amfs_destroy_dentry_cache();
	unregister_filesystem(&amfs_fs_type);
	pr_info("Completed amfs module unload\n");
}

MODULE_AUTHOR("Erez Zadok, Filesystems and Storage Lab, Stony Brook University"
	      " (http://www.fsl.cs.sunysb.edu/)");
MODULE_DESCRIPTION("Amfs " AMFS_VERSION
		   " (http://wrapfs.filesystems.org/)");
MODULE_LICENSE("GPL");

module_init(init_amfs_fs);
module_exit(exit_amfs_fs);

