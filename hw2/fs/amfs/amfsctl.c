#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <mntent.h>

#include "amfsctl.h"
#define MAX_PATTERN_LEN 256

void help_message(void){
	printf("Usage:\n");
	printf("./amfsctl -l/-a pattern/-r pattern mount_point\n");
	printf(" -l to List all patterns\n");
	printf(" -a pattern to add new pattern\n");
	printf(" -r pattern to remove a pattern\n");
	printf(" -h to shwo help message\n");
	printf(" mount_point is required!\n");
}
int is_mount_point(char* mount_point){
	/*need to check if mount_point is the mount point of amfs*/
	
	return 1;
}
int main(int argc, char **argv){
    
    char *mount_point;
	char *buf;
	int mode=0;
    int option = 0; 
    int err = -1;
    int fd;
	extern int optind, opterr, optopt;
	
    while ((option = getopt (argc, argv, ":la:r:h")) != -1)
	{
        err = 0;
        switch(option){
			case 'l':
				mode=0;
				break;
			case 'a':
				buf =optarg;
				mode=1;
				break;
			case 'r':
				buf =optarg;
				mode=2;
				break;
			case ':':
				printf("Option -%c needs argument\n",optopt);
				return 0;
			case '?':
				printf("Unknown option: -%c\n",optopt);
				return 0;
			case 'h':
				help_message();
				return 0;
			break;
			default:
			return 0;
			
		}
	}
	if(optind != argc-1){
		printf("Mount Point Required.\n");
		return 0;
	}
	
	mount_point=argv[optind];
	fd = open(mount_point, O_RDONLY);
	if (fd < 0) {
		printf("Cannot open %s\n",mount_point);
		return -1;
	}
	printf("mount point: %s\n",mount_point);
	
	if (!is_mount_point(mount_point))
		return -1;
	switch (mode){
		case 0:
			buf=malloc(4096);
			if(ioctl(fd, AMFS_IOCTLLIST, buf) < 0)
				printf("List error\n");
			else
				printf("PatternDB List: \n%s\n",buf);
			
			free(buf);
			break;
		case 1:
			err=ioctl(fd, AMFS_IOCTLADD, buf);
			if (err==0)
				printf("added %s\n",buf);
			else if (err==1)
				printf("%s exists\n",buf);
			else 
				printf("Add error\n");	
			break;
		case 2:
			err=ioctl(fd, AMFS_IOCTLREMOVE, buf);
			if (err==0)
				printf("%s not found\n",buf);
			else if (err==1)
				printf("removed %s\n",buf);
			else
				printf("Remove error\n");
			break;
		default: 
			return -1;
	}
	close(fd);
    return 0;
}
