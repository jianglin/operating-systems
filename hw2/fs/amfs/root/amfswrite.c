#include<stdio.h>

int main()
{
	
	FILE *f = fopen("writetest", "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		return 1;
	}
	const char *text = "line1\nline2\n3\nbad\ngood\n";
	fprintf(f, "Write test for AMFS:\n%s", text);
	fclose(f);
	return 0;
	
}

