# README #

### Linux Encrypt and Anti-Malware file system ###
* HW1 Add new system call to encrypt/decrypt file with CryptoAPI in Linux kernel
* HW2 Implement antimalware file system which calls malware detection before VFS read/write files
* HW3 Asynchronous system calls.



### Languages ###

* C 